# DOCKER SUR CENTOS8 - TP CLOUD

- Réseaux config en nat

- Ssh fonctionnel en se connectant avec "user"/"user"

- Installation de network manager pour `systemctl restart network .`

# INSTALLATION DE DOCKER

> ###   On installe d'abord le Dépôt docker

	$ sudo yum install -y yum-utils \  
	  device-mapper-persistent-data \  
      lvm2

On configure le dépôt stable :

     $ sudo yum-config-manager \ 
     --add-repo \ 
     [https://download.docker.com/linux/centos/docker-ce.repo]

> #### Installation de Docker CE

    sudo dnf -y install docker-ce --nobest
On active Docker par la suite avec 

    sudo systemctl start docker

Après l'installation on utilise la commande ``sudo docker info`` pour vérifier le bon fonctionnement de notre installation.

# MANIPULATION DU CONTENEUR

On télécharge l'image alpine :
- ``docker pull alpine``

On lance le conteneur :
- ``docker run alpine``

On lance un processus pour le conteneur alpine : 
- ``docker run alpine sleep 9999``

On liste avec la commande ``docker ps``

![](./Image/alpine.png)

> On rentre pour allouer un terminal dans le conteneur : 

``docker exec -it dreamy_colden sh`` 


On a bien ``"/#"`` => nous avons un shell dans notre conteneur.



On lance un > 

    Ls = bin    dev    etc    home   lib    media  mnt    opt    proc   root   run    sbin   srv    sys    tmp    usr    var (arborescence)

On lance

    ip a > 
    
On a la loopback et l'interface ``eth0@if13``

On lance un 
``Cat /etc/passwd``

Ca nous affiche tous les users dont root.

Avec la commande ``mount`` 

je vois bien mes points de montages. 

On détruit le conteneur : 
On le stop d'abord

``docker stop 07fb384d3b3a``

En suite on peut le détruire : 

``docker rm 07fb384d3b3a``

###### Conteneur nginx 

``Docker pull nginx``
On veut partager un port de l'hote ver le port 80 du conteneur
On lance  
``docker run -d -p 80:80 nginx``
En tapant *192.168.211.128:80* j'ai bien une page nginx

![](./Image/Nginx_web.png)


# Création d'image.
Je crée un dossier dans mon ``/home`` 

    Mkdir docker_docker
    Cd docker_docker 
    
On créé le docker file 

    Touch Dockerfile 
    
On édite le docker file:

    #Image parente sur laquelle se baser
    FROM alpine

    #Contenir Python
    RUN apk update \
    && apk add python3

    #port d'écoute
    EXPOSE 8888

    #Répertoire de travail
    WORKDIR /app

    #Récup fichier
    COPY /build/index.html /app/index.html

    #commande à lancer (Entrypoint sert à configurer un container au démarrage, et CMD est utilisé pour définir la commande de démarrage par défaut du conteneur)
    ENTRYPOINT [ "python3", "-m", "http.server" ]
    CMD [ "8888" ]

``docker build . -t python-web``

On partage le port de l'hote vers le port 8888 du conteneur 
docker run -d -p 7777:8888 python-web

``curl localhost:7777``

On a bien la page html.

La meme chose pour un volume docker avec ``-v``

``docker run -d -p 7778:8888 -v /home/user:/app  python-web``

    [user@localhost docker_docker]$ curl localhost:7778
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
    <html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Directory listing for /</title>
    </head>
    <body>
    <h1>Directory listing for /</h1>
    <hr>
    <ul>
    <li><a href=".bash_history">.bash_history</a></li>
    <li><a href=".bash_logout">.bash_logout</a></li>
    <li><a href=".bash_profile">.bash_profile</a></li>
    <li><a href=".bashrc">.bashrc</a></li>
    <li><a href=".viminfo">.viminfo</a></li>
    </ul>
    <hr>
    </body>
    </html>
    
# Modification du daemon : 

``vim /usr/lib/systemd/system/docker.service`` >> pour corriger 


``vim daemon.json`` >> on créé le fichier de conf : 

    {
        "hosts": ["unix:///var/run/docker.sock", "tcp://127.0.0.1:2376"]


    }

En faisant un ``systemctl status docker`` on a plusieurs choses : 

``localhost.localdomain dockerd[1396]: time="2020-01-27T03:27:10.279880944-05:00" level=info msg="API listen on 127.0.0.1:2376".``

##### Changement d'emplacement des données docker :

Le répertoire par défaut est dans :  ``var/lib/docker`` 
On créé dans home un répertoire :  ``data/docker``

Nous stoppons docker ``systemctl stop docker``
Nous modifions le daemon.json : 
    sudo vim etc/docker/daemon.json
   "data-root": "/home/data/docker",
   "storage-driver": "overlay",

On redémarre docker 
On va dans le répertoire ``data/docker``
On fait un ``sudo ls ``
On a bien l'arborescence 
       
$ sudo ls
builder  buildkit  containers  image  network  overlay  plugins  runtimes  swarm  tmp  trust  volumes

On rajoute dans le ``daemon.json`` cet

    "oom-score-adjust": -500

On prend cette valeur négatif qui assure quand cas de perte de Ram notre docker s'arrêtera.


# DOCKER COMPOSE

Installation de docker-compose 
``sudo curl -L "https://github.com/docker/compose/releases/download/1.25.3/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose``

On rajoute les droits :

``sudo chmod +x /usr/local/bin/docker-compose``

On test l'installation  

    docker-compose --version
    docker-compose version 1.25.3, build d4d1b42b.

# Write Your own

On créé un répertoire ``tp_compose``

On rajoute notre dockerfile dans le repertoire ``tp_compose``
		
		
On créé notre docker-compose.yml :
    version: "3.3"

    services:
     server:
        image: nginx
        networks:
        nginx-net:
            aliases:
            - nginx.test
            - nginx
    ports:
      - "8080:80"

    python-webapp:
        image: python-webapp
        build:
        context: .

    networks:
        nginx-net
        
        
        
On lance les conteneurs.
		
	Docker-compose up. 
	 On vérifie que la commande est bien passé : 
		
	    # docker-compose ps
		           Name                        Command              State     Ports
		---------------------------------------------------------------------------
		tp_compose_python-webapp_1   python3 -m http.server 8888   Exit 137
		tp_compose_server_1          nginx -g daemon off;          Exit 0


On créé un deuxième conteneur


    version: "3.3"

    services:
        server:
        image: nginx
        volumes:
      - "./nginx/test.docker.conf:/etc/nginx/conf.d/test-docker.conf"
      - "./nginx/certs/test.docker.crt:/certs/test.docker.crt"
      - "./nginx/certs/test.docker.key:/certs/test.docker.key"
    ports:
      - "443:443"
    networks:
      toto:
        aliases:
          - "server"
          - "server.b3"
          - "test.docker"

     python-webapp:
     image: python-webapp
        build:
      context: ./webapp
    networks:
      toto:
        aliases:
          - "python-webapp"

    networks:
        toto:

