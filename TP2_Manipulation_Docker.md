# TP2_Manipulation_Docker

## Gestion de conteneurs Docker :

Je vais mettre en évidence les 3 processus : 

        ps -ef | grep containerd
      root        976      1  0 10:00 ?        00:00:05 /usr/bin/containerd
      root       3564    976  0 10:47 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/7714cb24894b9e7f2e25424e8f34f6f799e3c4b15339e77dfbafa51138615c88 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
      root       3627   3295  0 10:50 pts/1    00:00:00 grep --color=auto containerd


On voit que le *PID* de ``containerd`` est 976 est que sont processus pére et le *PID* 1 (Kernel)

        ps -ef | grep dockerd
        root       1397      1  0 10:00 ?        00:00:01 /usr/bin/dockerd
        root       3315   3295  0 10:29 pts/1    00:00:00 grep --color=auto dockerd
        
        
On voit que le *PID* de ``dockerd`` est 1397 est que sont processus pére et le *PID* 1 (Kernel)

        ps -ef | grep containerd-shim
        root       3564    976  0 10:47 ?        00:00:00 containerd-shim -namespace moby -workdir /var/lib/containerd/io.containerd.runtime.v1.linux/moby/7714cb24894b9e7f2e25424e8f34f6f799e3c4b15339e77dfbafa51138615c88 -address /run/containerd/containerd.sock -containerd-binary /usr/bin/containerd -runtime-root /var/run/docker/runtime-runc
        root       3622   3295  0 10:47 pts/1    00:00:00 grep --color=auto containerd-shim
        

On voit que le *PID* de ``containerd-shim`` est 3564 est que sont processus pére et le *PID* 976 (contairned)

On récupére la liste des conteneur et des images

    curl --unix-socket /var/run/docker.sock http:/containers/json
    [{"Id":"36e40413d2c1b0f2a1e19262ee371f91ed101bf71dea78d1f096a6bcf34bbca6","Names":["/xenodochial_sutherland"],"Image":"alpine","ImageID":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Command":"sleep 9999","Created":1578396671,"Ports":[],"Labels":{},"State":"running","Status":"Up 8 minutes","HostConfig":{"NetworkMode":"default"},"NetworkSettings":{"Networks":{"bridge":{"IPAMConfig":null,"Links":null,"Aliases":null,"NetworkID":"f76f0ed7a51817135cfa639f9ebdb3d593248dc463094883516433c446728833","EndpointID":"f05c98e28f1fd33e9db0ce30eee65658e356d72b2452c4cb9d851a09c125719d","Gateway":"172.17.0.1","IPAddress":"172.17.0.2","IPPrefixLen":16,"IPv6Gateway":"","GlobalIPv6Address":"","GlobalIPv6PrefixLen":0,"MacAddress":"02:42:ac:11:00:02","DriverOpts":null}}},"Mounts":[]}

    curl --unix-socket /var/run/docker.sock http:/images/json
    [{"Containers":-1,"Created":1577215212,"Id":"sha256:cc0abc535e36a7ede71978ba2bbd8159b8a5420b91f2fbc520cdf5f673640a34","Labels":null,"ParentId":"","RepoDigests":["alpine@sha256:2171658620155679240babee0a7714f6509fae66898db422ad803b951257db78"],"RepoTags":["alpine:latest"],"SharedSize":-1,"Size":5591300,"VirtualSize":5591300}]


## SandBoxing

#### Namespace 

>> On cherche les namespaces du shell 

    # ls -al /proc/$$/ns
    total 0
    dr-x--x--x. 2 root root 0  9 févr. 11:11 .
    dr-xr-xr-x. 9 root root 0  9 févr. 10:28 ..
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 ipc -> 'ipc:[4026531839]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 mnt -> 'mnt:[4026531840]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 net -> 'net:[4026531992]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 pid -> 'pid:[4026531836]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 pid_for_children -> 'pid:[4026531836]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 user -> 'user:[4026531837]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:11 uts -> 'uts:[4026531838]'




On créé un pseudo conteneur avec ``unshare``

    ps -ef | grep bash
    user       1893    983  0 10:05 tty1     00:00:00 -bash
    user       1947   1927  0 10:06 pts/0    00:00:00 -bash
    root       2471   2467  0 10:16 pts/0    00:00:00 bash
    user       2964   2945  0 10:20 pts/1    00:00:00 -bash
    root       3295   3291  0 10:28 pts/1    00:00:00 bash
    [root@localhost ~]# unshare

``unshare`` doit éxécuter bash

    ps -ef | grep bash
    user       1893    983  0 10:05 tty1     00:00:00 -bash
    user       1947   1927  0 10:06 pts/0    00:00:00 -bash
    root       2471   2467  0 10:16 pts/0    00:00:00 bash
    user       2964   2945  0 10:20 pts/1    00:00:00 -bash
    root       3295   3291  0 10:28 pts/1    00:00:00 bash
    root       3792   3295  0 11:21 pts/1    00:00:00 -bash
    
>> Le processus doit utiliser des namespaces différents de notre hôte

``unshare -fmnpUr``

Les options ``f`` et ``r`` pour éviter des erreurs.

    ps -edf |grep bash
    nobody     1893    983  0 10:05 tty1     00:00:00 -bash
    nobody     1947   1927  0 10:06 pts/0    00:00:00 -bash
    root       2471   2467  0 10:16 pts/0    00:00:00 bash
    nobody     2964   2945  0 10:20 pts/1    00:00:00 -bash
    root       3295   3291  0 10:28 pts/1    00:00:00 bash
    root       3792   3295  0 11:21 pts/1    00:00:00 -bash
    root       3819   3818  0 11:29 pts/1    00:00:00 -bash
    [root@localhost ~]# ls -al /proc/$$/ns
    dr-x--x--x. 2 root root 0  9 févr. 11:30 .
    dr-xr-xr-x. 9 root root 0  9 févr. 10:00 ..
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 ipc -> 'ipc:[4026531839]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 mnt -> 'mnt:[4026531840]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 net -> 'net:[4026531992]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 pid -> 'pid:[4026531836]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 pid_for_children -> 'pid:[4026531836]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 user -> 'user:[4026531837]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:30 uts -> 'uts:[4026531838]'

**Le bash isolé :**

    ls -al /proc/3819/ns
    total 0
    dr-x--x--x. 2 root root 0  9 févr. 11:32 .
    dr-xr-xr-x. 9 root root 0  9 févr. 11:29 ..
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 ipc -> 'ipc:[4026531839]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 mnt -> 'mnt:[4026532764]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 net -> 'net:[4026532767]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 pid -> 'pid:[4026532765]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 pid_for_children -> 'pid:[4026532765]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 user -> 'user:[4026532763]'
    lrwxrwxrwx. 1 root root 0  9 févr. 11:32 uts -> 'uts:[4026531838]'
    

>> Avec docker (je prend une image alpine déjà présente)

``docker run -d alpine sleep 9999``

    docker ps
    CONTAINER ID        IMAGE               COMMAND                  CREATED              STATUS              PORTS                NAMES
    d26e412f2ad4        alpine              "sleep 9999"             About a minute ago   Up About a minute                        adoring_stallman
    35ff8e078d62        nginx               "nginx -g 'daemon of…"   51 minutes ago       Up About an hour    0.0.0.0:80->80/tcp   keen_sinoussi
    7714cb24894b        alpine              "sleep 9999"             About an hour ago    Up About an hour                         modest_liskov
    
On cherche le namespace pour le 1er container.

    docker inspect -f '{{.State.Pid}}' d26e412f2ad4
    3910
    [root@localhost home]# ls -al /proc/3910/ns
    total 0
    dr-x--x--x. 2 root root 0  9 févr. 12:00 .
    dr-xr-xr-x. 9 root root 0  9 févr. 12:00 ..
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 cgroup -> 'cgroup:[4026531835]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 ipc -> 'ipc:[4026532830]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 mnt -> 'mnt:[4026532828]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:00 net -> 'net:[4026532833]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 pid -> 'pid:[4026532831]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 pid_for_children -> 'pid:[4026532831]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 user -> 'user:[4026531837]'
    lrwxrwxrwx. 1 root root 0  9 févr. 12:05 uts -> 'uts:[4026532829]'


On utilise ``nsenter`` pour rentrer dans les namespaces de notre conteneur en y exécutant un shell

    nsenter --target 3910  --mount --uts --ipc --net --pid
    nsenter: impossible d'ouvrir /proc/3910/ns/ipc: Permission non accordée

>> je repasserai sur cet étape plus tard 

#### Cgroups
    
J'ai 3 conteneurs de lancé :
en faisant ``sudo systemd-cgtop``

    Control Group                                                                                     Tasks   %CPU   Memory  Input/s Output/s
    /                                                                                                   221      -     1.1G        -        -
    /docker                                                                                               4      -    24.6M        -        -
    /docker/35ff8e078d62954a7a582fff8481fabe6f7e792278de6eb76efa14bef00636f6                              2      -     2.2M        -        -
    /docker/7714cb24894b9e7f2e25424e8f34f6f799e3c4b15339e77dfbafa51138615c88                              1      -   544.0K        -        -
    /docker/d26e412f2ad4ef64a18ff402f4223a78132dd2d79c919ffc7ab35900455d871b                              1      -     1.2M        -        -
    /system.slice                                                                                        95      -   669.5M        -        -
    /system.slice/NetworkManager.service                                                                  3      -     8.0M        -        -
    /system.slice/auditd.service                                                                          2      -     4.9M        -        -
    
    
Les 3 conteneurs sont lancés dans :   
- ``/docker/35ff8e078d62954a7a582fff8481fabe6f7e792278de6eb76efa14bef00636f6`` 
- ``/docker/7714cb24894b9e7f2e25424e8f34f6f799e3c4b15339e77dfbafa51138615c88``
- ``/docker/d26e412f2ad4ef64a18ff402f4223a78132dd2d79c919ffc7ab35900455d871b``

Je ferme 2 de ces conteneurs avec ``docker stop``.

On lance un conteneur docker pour travailler avec.
    docker ps
    CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
    d26e412f2ad4        alpine              "sleep 9999"        30 minutes ago      Up 30 minutes                           adoring_stallman

On a celui la qui tourne.

On veut ca mémoire Ram Max autorisé : 

    cat /sys/fs/cgroup/memory/docker/d26e412f2ad4ef64a18ff402f4223a78132dd2d79c919ffc7ab35900455d871b/memory.limit_in_bytes
    9223372036854771712
    
On veut le nombre de processus qu'il peut contenir :

     cat /sys/fs/cgroup/cpu/docker/d26e412f2ad4ef64a18ff402f4223a78132dd2d79c919ffc7ab35900455d871b/cpu.shares
    1024
    
On change les valeurs cgroups allouées par défaut avec des options de la commandes ``docker run`` 


    docker run -d -p 8080:6666 --cpu-shares 50 alpine sleep 99999
    b4eaca1f21c8f02c3b384311aaaedfd21946882d7939056d9e24ad57c53ebf97

On vérifie

    cat /sys/fs/cgroup/cpu/docker/b4eaca1f21c8f02c3b384311aaaedfd21946882d7939056d9e24ad57c53ebf97/cpu.shares
    50

    docker run -d -p 8880:5556 --memory 1024000000 alpine sleep 99999                                                    
    d2205e78b21ce7cd4bb7e8fa21644359ecff82528fe58b77ee95f707d35be262

    
On vérifie 

     cat /sys/fs/cgroup/memory/docker/d2205e78b21ce7cd4bb7e8fa21644359ecff82528fe58b77ee95f707d35be262/memory.limit_in_bytes
     1024000000



On détermine les **capabilities** actuellement utilisées par notre shell

     capsh --print
    Current: = cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read+ep
    Bounding set =cap_chown,cap_dac_override,cap_dac_read_search,cap_fowner,cap_fsetid,cap_kill,cap_setgid,cap_setuid,cap_setpcap,cap_linux_immutable,cap_net_bind_service,cap_net_broadcast,cap_net_admin,cap_net_raw,cap_ipc_lock,cap_ipc_owner,cap_sys_module,cap_sys_rawio,cap_sys_chroot,cap_sys_ptrace,cap_sys_pacct,cap_sys_admin,cap_sys_boot,cap_sys_nice,cap_sys_resource,cap_sys_time,cap_sys_tty_config,cap_mknod,cap_lease,cap_audit_write,cap_audit_control,cap_setfcap,cap_mac_override,cap_mac_admin,cap_syslog,cap_wake_alarm,cap_block_suspend,cap_audit_read
    Ambient set =
    Securebits: 00/0x0/1'b0
     secure-noroot: no (unlocked)
    secure-no-suid-fixup: no (unlocked)
     secure-keep-caps: no (unlocked)
     secure-no-ambient-raise: no (unlocked)
    uid=0(root)
    gid=0(root)
    groups=0(root)

On détermine les **capabilities** du processus lancé par un conteneur Docker

     cat /proc/3910/status | grep Cap
    CapInh: 00000000a80425fb
    CapPrm: 00000000a80425fb
    CapEff: 00000000a80425fb
    CapBnd: 00000000a80425fb
    CapAmb: 0000000000000000


Jouons avec **PING**

Le chemin absolu de ping

    /usr/bin/ping
    
On récupére la liste de ses **capabilities**

    getcap /usr/bin/ping
    /usr/bin/ping = cap_net_admin,cap_net_raw+p
    
    
On lance ``setcap '' /usr/bin/ping`` pour enlever toutes ses **capabilities** par une liste vide 

Pn vérifie le ping : 

     ping 127.0.0.1
    ping: socket: Operation not permitted
    

On voit avec ``strace`` que c'est l'accés **ICMP** qui est enlevé.
    strace ping 127.0.0.1
    [...]
    socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP) = -1 EACCES (Permission denied)
    socket(AF_INET, SOCK_RAW, IPPROTO_ICMP) = -1 EPERM (Operation not permitted)
    [...]



>> ON lance un conteneur NGINX qui a le strict nécessaire de **capabilities** pour fonctionner

    sudo docker run -p 80:80 --cap-drop all --cap-add=chown --cap-add=net_bind_service --cap-add=setuid --cap-add=setgid -d nginx
    98d8146cd3f2485d06799471ef40e342f25bd51431038571f9884fe161c79586
    [root@localhost ~]# docker ps
    CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
    98d8146cd3f2        nginx               "nginx -g 'daemon of…"   29 seconds ago      Up 26 seconds       0.0.0.0:80->80/tcp       pensive_bhabha
    

Il a besoin de :
- pouvoir changer l'appartenance à des fichiers. D'où le rajout de la ``CAP_CHOWN``
- Pour pourvoir *bind* le port, il faut ``CAP_NET_BIND_SERVICE``
- pour manipuler les processus il a besoin de ``CAP_SETUID`` et ``CAP_SETGID``




